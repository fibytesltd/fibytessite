<?php

echo '<meta charset="utf-8">';
echo '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
echo '<link rel="stylesheet" href="css/animate.css">';
echo '<link rel="stylesheet" href="fonts/icomoon/style.css">';
echo '<link rel="stylesheet" href="css/bootstrap.css">';
echo '<link rel="stylesheet" href="css/style.css">';
echo '<link rel="stylesheet" href="css/demo.css">';
echo '<link rel="stylesheet" href="css/footer-distributed.css">';
echo '<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">';
echo '<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:400,700|Fira+Sans:400,500,600,700" rel="stylesheet">';
echo '<link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">';


?>