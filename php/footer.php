<? php
echo '<footer class="footer-distributed">';
echo '<div class="footer-right">';

echo '<p class="footer-links">';
echo '<ul style="list-style-type:none; ">';
echo '<li><a href="index.html">HOME</a></li>';

echo '<li><a href="services.html">SERVICES</a></li>';

echo '<li><a href="about.html">ABOUT</a></li>';

echo '<li><a href="contact.html">CONTACT</a></li>';
echo '</ul>';
echo '</p>';
echo '</div>';
echo '<div class="footer-left">';
echo '<a class="navbar-brand" href="index.html"><img src="images/Font1-footer.png" alt="Fibytes" width='182.69' height="58.08"></a>';

echo '<p class="footer-company-motto">Taking the complexity out of marketing.</p>';
echo '</div>';
echo '<br />';
echo '<br />';
echo '<p class="footer-company-name" style="text-align:center">Fibytes LTD. &copy; 2018</p>';
echo '</footer>';
?>