<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="fonts/icomoon/style.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/demo.css">
<link rel="stylesheet" href="css/footer-distributed.css">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:400,700|Fira+Sans:400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">


</head>

<body  data-spy="scroll" data-target="#pb-navbar" data-offset="200">
  <?php include('navigation.php'); ?>

<section class="site-hero"  id="section-home"  style="margin:3vh; margin-bottom: 5vh">

  
    <div class="container" >
     
        
            <img src="images/bannerImage-textFinalBlack.png" style="width: 95%; height: 95%; margin-left: 2vw">   
          

          
        
     
    </div>
    
    

      
    

  


</section>
<br />
<br />
<br />


<div class="section-heading2 text-center"><h2></h2></div>
<p class="lead site-subheading mb-4 site-animate text-center" p style="font-size: 32px; color: #586674;  font-family: Arial, Helvetica, sans-serif !important; font-weight: 300;"  >Marketing tailored to your needs.<br /><br /></p>	

<div class="container" style="margin-top:4vh; ">
 
    <div class="row" >
      <div class="col lg-3"><img src="images/webDevHome.svg" style=" max-width:95%; max-height:95%; margin-left: 11px"></div>
      <div class="col lg-1"></div>
      <div class="col lg-3"><img src="images/engageHome.svg" style=" max-width:95%; max-height:95%;  margin-left: 11px"></div>
      <div class="col lg-1"></div>
      <div class="col lg-3"><img src="images/seoHome.svg" style=" max-width:95%; max-height:95%;  margin-left: 11px"></div>
    </div>
    <div class="row" style="padding-top: 0%; font-size: 225%; font-weight: 500; text-align: center">
        <div class="col lg-4" style="margin:auto; text-align: center"><p>Develop</p></div>
        <div class="col lg"></div>
        <div class="col lg-4" style="margin:auto; text-align: center"><p>Engage</p></div>
        <div class="col lg"></div>
        <div class="col lg-4" style="margin:auto; text-align: center"><p>Optimize</p></div>
    </div>

    <div class="row" style="padding-top: 0%; font-size: 150%; font-weight: 300; text-align: center">
        <div class="col lg-4" style="margin:auto; text-align: center"><p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mattis.</strong></p></div>
        <div class="col lg"></div>
        <div class="col lg-4" style="margin:auto; text-align: center"><p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mattis.</strong></p></div>
        <div class="col lg"></div>
        <div class="col lg-4" style="margin:auto; text-align: center"><p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mattis.</strong></p></div>
    </div>
    
    
</div>


<footer class="footer-distributed">

  

    <div class="footer-right">
    
    <p class="footer-links">
      <ul style="list-style-type:none; ">
      <li><a href="index.html">HOME</a></li>
      
      <li><a href="services.html">SERVICES</a></li>
      
      <li><a href="about.html">ABOUT</a></li>
      
      <li><a href="contact.html">CONTACT</a></li>
    </ul>
    </p>
  </div>
  <div class="footer-left">
  <a class="navbar-brand" href="index.html"><img src="images/Font1-footer.png" alt="Fibytes" width='182.69' height="58.08"></a>
             
  <p class="footer-company-motto">Taking the complexity out of marketing.</p>
  </div>
  <br />
  <br />
  <p class="footer-company-name" style="text-align:center">Fibytes LTD. &copy; 2018</p>
  </footer>
  


</body>
</html>


